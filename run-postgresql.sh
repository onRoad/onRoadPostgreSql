# Check if there's an active container
	echo "Checking if exists a onRoadPostgreSql container..."
	DOCKER_CONTAINER_ONROAD_POSTGRESQL_ID=$(docker ps | grep "onRoadPostgreSql" | sed -e 's/^\(.\{12\}\).*/\1/')

	# Check if there is an active container
	if [ ! -z "$DOCKER_CONTAINER_ONROAD_POSTGRESQL_ID" ]; then
		echo "There are active containers. Deleting..."
		docker rm -f $(docker ps | grep "onRoadPostgreSql" | sed -e 's/^\(.\{12\}\).*/\1/')
	fi

echo "Creating new containers..."
docker-compose up -d